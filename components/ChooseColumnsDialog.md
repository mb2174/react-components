### Examples

```js
import * as React from 'react';

import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';

// The ChooseColumnsDialog component expects a list of possible columns each with
// a unique key. The order of this array determines the order columns appear in
// the "add column" UI.
const ALL_COLUMNS = [
  {
    key: 'name',
    primaryText: 'Name',
  },
  {
    key: 'age',
    primaryText: 'Age',
    secondaryText: 'Age in years',
  },
  {
    key: 'address',
    primaryText: 'Address',
    secondaryText: 'Address (if given)',
  },
  {
    key: 'since',
    primaryText: 'Member since',
    secondaryText: 'Date of joining site',
  },
  {
    key: 'favouriteColour',
    primaryText: 'Favourite colour',
    secondaryText: 'Preferred colour of bikeshed',
  },
];

// Create a map between column key and the column.
const columnMap = new Map(ALL_COLUMNS.map(column => [column.key, column]));

// Maintain a flag indicating if the dialog should be shown.
const [isOpen, setIsOpen] = React.useState(false);

// The selected column keys are maintained as a state.
const [selectedKeys, setSelectedKeys] = React.useState(['age', 'since']);

// The fixed column keys are constant.
const fixedKeys = ['name'];

<>
  <Box py={1}>{
    [...fixedKeys, ...selectedKeys].map((key, index) => (
      <Box key={index} px={1} display="inline">
        <Chip label={columnMap.get(key).primaryText} />
      </Box>
    ))
  }</Box>
  <Button onClick={() => setIsOpen(true)}>Customise columns</Button>
  <ChooseColumnsDialog
    open={isOpen}

    onClose={() => setIsOpen(false)}
    onCancel={() => setIsOpen(false)}
    onSetColumns={(newKeys) => { setSelectedKeys(newKeys); setIsOpen(false); }}

    columns={ALL_COLUMNS}
    fixedColumnKeys={fixedKeys}
    initialSelectedColumnKeys={selectedKeys}

    DialogProps={{
      fullWidth: true,
      maxWidth: 'sm',
    }}
  />
</>;
```
