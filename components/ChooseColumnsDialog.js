"use strict";
/**
 * ChooseColumnsDialog allows selection of column display and ordering for tables.
 *
 * The component manages its own UI state and communicates via a series of event handler props.
 * Users of the component do not get notified as the user re-orders and adds columns, they only get
 * notified once the user has selected the layout.
 *
 * See the individual prop descriptions for more information.
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
var React = require("react");
var Box_1 = require("@material-ui/core/Box");
var Button_1 = require("@material-ui/core/Button");
var Dialog_1 = require("@material-ui/core/Dialog");
var DialogActions_1 = require("@material-ui/core/DialogActions");
var DialogContent_1 = require("@material-ui/core/DialogContent");
var DialogTitle_1 = require("@material-ui/core/DialogTitle");
var IconButton_1 = require("@material-ui/core/IconButton");
var List_1 = require("@material-ui/core/List");
var ListItem_1 = require("@material-ui/core/ListItem");
var ListItemIcon_1 = require("@material-ui/core/ListItemIcon");
var ListItemSecondaryAction_1 = require("@material-ui/core/ListItemSecondaryAction");
var ListItemText_1 = require("@material-ui/core/ListItemText");
var MenuItem_1 = require("@material-ui/core/MenuItem");
var Select_1 = require("@material-ui/core/Select");
var Close_1 = require("@material-ui/icons/Close");
var DragIndicator_1 = require("@material-ui/icons/DragIndicator");
var Lock_1 = require("@material-ui/icons/Lock");
var styles_1 = require("@material-ui/core/styles");
var react_smooth_dnd_1 = require("react-smooth-dnd");
var lodash_1 = require("lodash");
var styles = function (theme) { return styles_1.createStyles({
    dragHandle: {
        '&:hover': {
            color: theme.palette.text.primary,
            cursor: 'grab'
        }
    },
    dragGhost: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[2],
        cursor: 'grab',
        '& $dragHandle': {
            color: theme.palette.text.primary
        }
    },
    container: {
        // Override minimum height set by smooth-dnd.
        minHeight: 0
    },
    dialogContent: {
        scrollBehavior: 'smooth'
    },
    hidden: { display: 'none' },
    addColumn: {
        paddingTop: theme.spacing(1),
        paddingLeft: theme.spacing(4.5),
        paddingRight: theme.spacing(4),
        minHeight: 30,
        overflowY: 'hidden'
    },
    selectedColumns: {
        paddingTop: 0,
        paddingBottom: 0
    }
}); };
;
exports.ChooseColumnsDialog = styles_1.withStyles(styles)(function (_a) {
    var classes = _a.classes, open = _a.open, _b = _a.onClose, onClose = _b === void 0 ? function () { return null; } : _b, _c = _a.onCancel, onCancel = _c === void 0 ? function () { return null; } : _c, _d = _a.onSetColumns, onSetColumns = _d === void 0 ? function (selectedKeys) { return null; } : _d, _e = _a.columns, columns = _e === void 0 ? [] : _e, _f = _a.initialSelectedColumnKeys, initialSelectedColumnKeys = _f === void 0 ? [] : _f, _g = _a.fixedColumnKeys, fixedColumnKeys = _g === void 0 ? [] : _g, DialogProps = _a.DialogProps;
    // Create state values and setters for same.
    var _h = React.useState([]), selectedKeys = _h[0], setSelectedKeys = _h[1];
    // If columns prop changes, maintain columnsByKey as a map from column key to the underlying
    // Column.
    var columnsByKey = React.useMemo(function () { return (lodash_1.fromPairs(columns.map(function (column) { return [column.key, column]; }))); }, [columns]);
    // Reset the selectedKeys state if the initialSelectedColumnKeys prop changes. We don't use
    // useMemo here since we also want to manage selectedKeys elsewhere.
    React.useEffect(function () { return setSelectedKeys(initialSelectedColumnKeys); }, [initialSelectedColumnKeys]);
    // If selected or fixed columns array changes, update unselected array to be all columns *not* in
    // selected or fixed columns. Importantly, we respect the order of the columns prop.
    var unselectedKeys = React.useMemo(function () {
        // Form a set of all the selected or fixed keys.
        var selectedOrFixedKeys = new Set(__spreadArrays(fixedColumnKeys, selectedKeys));
        // Update the unselected keys map with those not in the selectedOrFixedKeys set. Make sure that
        // the order matches the columns prop.
        return (columns
            .filter(function (_a) {
            var key = _a.key;
            return !selectedOrFixedKeys.has(key);
        })
            .map(function (_a) {
            var key = _a.key;
            return key;
        }));
    }, [fixedColumnKeys, selectedKeys, columns]);
    // Reference for Dialog Content (to be scrolled)
    var contentRef = React.createRef();
    // Whether a new column has just been added
    var _j = React.useState(false), columnAdded = _j[0], setColumnAdded = _j[1];
    // Trigger scroll to bottom when new column added
    React.useEffect(function () {
        if (columnAdded) {
            if (contentRef.current) {
                contentRef.current.scrollTop = contentRef.current.scrollHeight;
            }
            setColumnAdded(false);
        }
    }, [columnAdded, contentRef]);
    // Convenience functions to add and remove keys to/from the selected keys array.
    var selectColumnKey = function (key) {
        setSelectedKeys(__spreadArrays(selectedKeys, [key]));
        // Also trigger scroll to bottom
        setColumnAdded(true);
    };
    var deselectColumnKey = function (key) { return setSelectedKeys(selectedKeys.filter(function (k) { return key !== k; })); };
    // Handle re-ordering via drag and drop.
    var handleDrop = function (_a) {
        var removedIndex = _a.removedIndex, addedIndex = _a.addedIndex;
        // Don't do anything if the item wasn't simply moved from one index to another.
        if ((removedIndex === null) || (addedIndex === null)) {
            return;
        }
        // Determine which column key was removed.
        var removedKey = selectedKeys[removedIndex];
        // Remove the key from the array.
        var remainingKeys = __spreadArrays(selectedKeys.slice(0, removedIndex), selectedKeys.slice(removedIndex + 1));
        // Add the removed key into the addedIndex position.
        setSelectedKeys(__spreadArrays(remainingKeys.slice(0, addedIndex), [
            removedKey
        ], remainingKeys.slice(addedIndex)));
    };
    return React.createElement(Dialog_1["default"], __assign({}, DialogProps, { open: open, onClose: onClose, onExited: function () { return setSelectedKeys(initialSelectedColumnKeys); }, "aria-labelledby": "choose-columns-dialog-title" }),
        React.createElement(DialogTitle_1["default"], { id: "choose-columns-dialog-title" }, "Customise Table Columns"),
        React.createElement(DialogContent_1["default"], { dividers: true, ref: contentRef, className: classes.dialogContent },
            React.createElement(List_1["default"], null,
                // List the fixed columns first.
                fixedColumnKeys
                    .map(function (k) { return columnsByKey[k]; })
                    .filter(function (column) { return Boolean(column); })
                    .map(function (column, index) { return (React.createElement(ListItem_1["default"], { key: index, className: classes.selectedColumns },
                    React.createElement(ListItemIcon_1["default"], null,
                        React.createElement(Box_1["default"], { color: "text.hint" },
                            React.createElement(Lock_1["default"], null))),
                    React.createElement(ListItemText_1["default"], { primary: column.primaryText, secondary: column.secondaryText }))); }),
                React.createElement(react_smooth_dnd_1.Container, { dragClass: classes.dragGhost, dragHandleSelector: '.' + classes.dragHandle, lockAxis: "y", onDrop: handleDrop, render: function (ref) { return (React.createElement("div", { ref: ref, className: classes.container }, 
                    // Render the selected list items. Make sure to give the drag handle the correct class
                    // to be recognised as the handle.
                    selectedKeys
                        .map(function (k) { return columnsByKey[k]; })
                        .filter(function (column) { return Boolean(column); })
                        .map(function (column, index) { return (React.createElement(react_smooth_dnd_1.Draggable, { key: index },
                        React.createElement(ListItem_1["default"], { key: index, className: classes.selectedColumns },
                            React.createElement(ListItemIcon_1["default"], { className: classes.dragHandle },
                                React.createElement(DragIndicator_1["default"], null)),
                            React.createElement(ListItemText_1["default"], { primary: column.primaryText, secondary: column.secondaryText }),
                            React.createElement(ListItemSecondaryAction_1["default"], null,
                                React.createElement(IconButton_1["default"], { edge: "end", onClick: function () { return deselectColumnKey(column.key); } },
                                    React.createElement(Close_1["default"], null)))))); }))); } }))),
        /* Only show the add column UI if there are unselected columns. */
        (unselectedKeys.length > 0) &&
            React.createElement(DialogContent_1["default"], { classes: { root: classes.addColumn }, dividers: true },
                React.createElement(Select_1["default"], { displayEmpty: true, fullWidth: true, id: "add-column-select", onChange: function (event) { return selectColumnKey(event.target.value); }, value: "" },
                    React.createElement(MenuItem_1["default"], { value: "", disabled: true, classes: { root: classes.hidden } },
                        React.createElement(Box_1["default"], { color: "text.secondary" }, "Add column")),
                    // Only show columns which aren't selected in the list.
                    unselectedKeys
                        .map(function (k) { return columnsByKey[k]; })
                        .filter(function (column) { return Boolean(column); })
                        .map(function (column, index) { return (React.createElement(MenuItem_1["default"], { key: index, value: column.key }, column.primaryText)); }))),
        React.createElement(DialogActions_1["default"], null,
            React.createElement(Button_1["default"], { onClick: function () { return onCancel(); } }, "Cancel"),
            React.createElement(Button_1["default"], { color: "primary", onClick: function () { return onSetColumns(selectedKeys); } }, "Set Columns")));
});
exports["default"] = exports.ChooseColumnsDialog;
