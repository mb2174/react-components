"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
var React = require("react");
var styles_1 = require("@material-ui/core/styles");
var core_1 = require("@material-ui/core");
var ArrowDropDown_1 = require("@material-ui/icons/ArrowDropDown");
var lodash_1 = require("lodash");
// the style for the global context filter
var styles = function (theme) { return styles_1.createStyles({
    buttonLabel: {
        marginLeft: theme.spacing(1),
        textTransform: 'none'
    },
    edgeStart: {
        marginLeft: -theme.spacing(2)
    },
    edgeEnd: {
        marginRight: -theme.spacing(2)
    }
}); };
/**
 * The default filter option value
 */
var DEFAULT_SELECT_VALUE = ' ';
/**
 * Renders a global context filter and handles the application of that filter.
 */
exports.GlobalContextFilter = styles_1.withStyles(styles)(function (_a) {
    var label = _a.label, options = _a.options, selectedOptionId = _a.selectedOptionId, _b = _a.onOptionSelected, onOptionSelected = _b === void 0 ? function () { return null; } : _b, edge = _a.edge, classes = _a.classes;
    var _c = React.useState(null), anchorEl = _c[0], setAnchorEl = _c[1];
    var allOptions = __spreadArrays([{ id: DEFAULT_SELECT_VALUE, description: label }], options);
    var option = options.find(function (option) { return selectedOptionId === option.id; });
    var optionId = selectedOptionId ? selectedOptionId : DEFAULT_SELECT_VALUE;
    var buttonRootClass = [
        (edge === "start") ? classes.edgeStart : '',
        (edge === "end") ? classes.edgeEnd : '',
    ].join(' ');
    var menuId = lodash_1.kebabCase(label) + "-menu";
    return (React.createElement(React.Fragment, null,
        React.createElement(core_1.Button, { "aria-controls": menuId, "aria-haspopup": "true", color: "inherit", classes: {
                root: buttonRootClass,
                label: classes.buttonLabel
            }, onClick: function (event) { return setAnchorEl(event.currentTarget); }, disableRipple: true, disableFocusRipple: true },
            option ? option.description : label,
            React.createElement(ArrowDropDown_1["default"], null)),
        React.createElement(core_1.Menu, { id: menuId, anchorEl: anchorEl, keepMounted: true, open: Boolean(anchorEl), onClose: function () { return setAnchorEl(null); }, anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'right'
            }, transformOrigin: {
                vertical: 'top',
                horizontal: 'right'
            }, getContentAnchorEl: null }, allOptions.map(function (option) { return (React.createElement(core_1.MenuItem, { key: option.id, selected: optionId === option.id, onClick: function () {
                onOptionSelected(option.id === DEFAULT_SELECT_VALUE ? undefined : option.id);
                setAnchorEl(null);
            } }, option.description)); }))));
});
exports["default"] = exports.GlobalContextFilter;
